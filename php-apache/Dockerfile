FROM php:7.1-apache

ENV DEBIAN_FRONTEND noninteractive

RUN echo "\e[21;92m-----INSTALLING PACKAGES-----\e[0m"

RUN apt-get update && apt-get install -y \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libpng-dev \
    libmcrypt-dev \
    apache2-bin \
    curl \
    git \
    vim \
    libxml2-dev \
    libicu-dev \
    elinks \
    openssh-server \
    unzip

RUN docker-php-ext-install \ 
    pdo_mysql \
    mysqli \
    mbstring \
    mcrypt \
    gd \
    iconv \
    zip \
    xml \
    intl

RUN echo "\e[21;92m-----CONFIGURING APACHE & PHP-----\e[0m"

ADD ./php-apache/php.ini /usr/local/etc/php/php.ini

ADD ./php-apache/default.conf /etc/apache2/sites-available/000-default.conf

ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP root
ENV APACHE_LOG_DIR /var/log/apache2
ENV APACHE_PID_FILE /var/run/apache2.pid
ENV APACHE_RUN_DIR /var/run/apache2
ENV APACHE_LOCK_DIR /var/lock/apache2

RUN mkdir -p $APACHE_RUN_DIR $APACHE_LOCK_DIR $APACHE_LOG_DIR
RUN a2enmod rewrite

RUN echo "\e[21;92m-----SETTING UP ENTRYPOINT SCRIPT-----\e[0m"

COPY ./php-apache/entrypoint.sh /php-apache/entrypoint.sh
CMD /php-apache/entrypoint.sh

RUN echo "\e[21;92m-----SETTING UP SSHDEV-----\e[0m"

RUN useradd sshdev
RUN printf "1racecar\n1racecar" | passwd sshdev
RUN usermod -aG sudo sshdev
COPY sshd_config /etc/sshd/
RUN chmod -R 777 /var/www/html
