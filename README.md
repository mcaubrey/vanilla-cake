# CakePHP 2.10.12 in a Docker Container (Arleady Setup)

This is CakePHP 2.10.12 setup to run using docker. All things being equal, you should only need to run `docker-compose up -d` to get started.

This container has also been setup to be accessible via ssh.
